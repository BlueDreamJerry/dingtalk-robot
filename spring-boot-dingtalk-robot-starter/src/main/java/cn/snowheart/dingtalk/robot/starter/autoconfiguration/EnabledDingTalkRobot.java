package cn.snowheart.dingtalk.robot.starter.autoconfiguration;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Import;

/**
 * DingTalk机器人的Enable注解
 *
 * @author Wanxiang Liu
 * @version 1.0.0
 */
@Configurable
@Import(DingTalkRobotAutoConfiguration.class)
public @interface EnabledDingTalkRobot {
}