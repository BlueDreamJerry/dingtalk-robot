package cn.snowheart.dingtalk.robot.starter.autoconfiguration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 加载配置在application.yaml中的配置属性
 *
 * @author Wanxiang Liu
 * @version 1.0.0
 */
@ConfigurationProperties("dingtalk.robot")
public class DingTalkRobotProperties {

    /**
     * 钉钉机器人的WebHook地址
     */
    private String webhook;

    public String getWebhook() {
        return webhook;
    }

    public void setWebhook(String webhook) {
        this.webhook = webhook;
    }
}